-- Deploy servermodel:servermodel_schema to pg

BEGIN;

-- Création de la table Version
CREATE TABLE Version (
  VersionId SERIAL PRIMARY KEY,
  VersionName VARCHAR(255) NOT NULL,
  VersionDistribution VARCHAR(255) NOT NULL,
  VersionLabel VARCHAR(255),
  UNIQUE (VersionName, VersionDistribution)
);

-- Création de la table Release
CREATE TABLE Release (
  ReleaseId SERIAL PRIMARY KEY,
  ReleaseName VARCHAR(255) NOT NULL,
  ReleaseVersionId INTEGER NOT NULL,
  UNIQUE (ReleaseName, ReleaseVersionId),
  FOREIGN KEY (ReleaseVersionId) REFERENCES Version(VersionId)
);

-- Création de la table SubRelease
CREATE TABLE SubRelease (
  SubReleaseId SERIAL PRIMARY KEY,
  SubReleaseName VARCHAR(255) NOT NULL,
  SubReleaseReleaseId INTEGER NOT NULL,
  FOREIGN KEY (SubReleaseReleaseId) REFERENCES Release(ReleaseId)
);

-- Création de la table Source
CREATE TABLE Source (
  SourceId SERIAL PRIMARY KEY,
  SourceName VARCHAR(255) NOT NULL UNIQUE,
  SourceShasum VARCHAR(255),
  SourceURL TEXT
);

-- Création de la table ServerModel
CREATE TABLE ServerModel (
  ServerModelId SERIAL PRIMARY KEY,
  ServerModelName VARCHAR(255) NOT NULL,
  ServerModelDescription VARCHAR(255) NOT NULL,
  ServerModelSourceId INTEGER NOT NULL,
  ServerModelParentsId INTEGER [] DEFAULT '{}',
  ServerModelSubReleaseId INTEGER NOT NULL,
  ServerModelUsers hstore,
  FOREIGN KEY (ServerModelSourceId) REFERENCES Source(SourceId),
  FOREIGN KEY (ServerModelSubReleaseId) REFERENCES SubRelease(SubReleaseId),
  UNIQUE (ServerModelName, ServerModelSubReleaseId)
);

-- Création de la table ApplicationService
CREATE TABLE ApplicationService (
  ApplicationServiceId SERIAL PRIMARY KEY,
  ApplicationServiceName VARCHAR(255) NOT NULL,
  ApplicationServiceDescription VARCHAR(255) NOT NULL,
  ApplicationServiceSubReleaseId INTEGER NOT NULL,
  ApplicationServiceSourceId INTEGER NOT NULL,
  ApplicationServiceIsVirtual BOOLEAN DEFAULT FALSE,
  ApplicationServiceDependencies JSON,
  FOREIGN KEY (ApplicationServiceSubReleaseId) REFERENCES SubRelease(SubReleaseId),
  FOREIGN KEY (ApplicationServiceSourceId) REFERENCES Source(SourceId),
  UNIQUE (ApplicationServiceName, ApplicationServiceSubReleaseId)
);

-- Création de la table de jointure ApplicationServiceProvides
CREATE TABLE ApplicationServiceProvides (
  ApplicationServiceId INTEGER NOT NULL,
  VirtualApplicationServiceId INTEGER NOT NULL,
  FOREIGN KEY (ApplicationServiceId) REFERENCES ApplicationService(ApplicationServiceId),
  FOREIGN KEY (VirtualApplicationServiceId) REFERENCES ApplicationService(ApplicationServiceId),
  PRIMARY KEY (ApplicationServiceId, VirtualApplicationServiceId)
);

-- Création de la table Package
CREATE TABLE Package (
  PackageId SERIAL PRIMARY KEY,
  PackageApplicationServiceId INTEGER,
  PackageName VARCHAR(255) NOT NULL,
  FOREIGN KEY (PackageApplicationServiceId) REFERENCES ApplicationService(ApplicationServiceId)
);

-- Création de la table Document
CREATE TABLE Document (
  DocumentId SERIAL PRIMARY KEY,
  DocumentServiceId INTEGER,
  DocumentName VARCHAR(255) NOT NULL,
  DocumentPath TEXT,
  DocumentOwner VARCHAR(255) DEFAULT 'root',
  DocumentGroup VARCHAR(255) DEFAULT 'root',
  DocumentMode VARCHAR(10) DEFAULT '0644',
  DocumentType VARCHAR(100) CHECK ( DocumentType IN ('probes', 'aggregated_dico', 'dico', 'template', 'pretemplate', 'posttemplate', 'preservice', 'postservice', 'creolefuncs', 'file') ),
  DocumentSHASUM VARCHAR(255),
  DocumentContent BYTEA,
  FOREIGN KEY (DocumentServiceId) REFERENCES ApplicationService(ApplicationServiceId)
);


CREATE TABLE ServerModelApplicationService (
  ServerModelApplicationServiceId SERIAL PRIMARY KEY,
  ServerModelId INTEGER,
  ApplicationServiceId INTEGER,
  UNIQUE (ServerModelId, ApplicationServiceId),
  FOREIGN KEY (ServerModelId) REFERENCES ServerModel(ServerModelId),
  FOREIGN KEY (ApplicationServiceId) REFERENCES ApplicationService(ApplicationServiceId)
);

COMMIT;
