-- Revert servermodel:servermodel_schema from pg

BEGIN;

DROP SCHEMA servermodel CASCADE;

COMMIT;
