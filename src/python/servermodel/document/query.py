"""
"""
DOCUMENT_DELETE = """DELETE from document"""


def erase_document(cursor):
    cursor.execute(DOCUMENT_DELETE)
