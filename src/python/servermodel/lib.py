from .applicationservice import ApplicationService
from .servermodel.query import fetch_applicationservices_documents, \
                               fetch_servermodel_applicationservices, \
                               fetch_servermodel_applicationservice
from .applicationservice.query import insert_aggregated_dictionary, \
                                      update_document, \
                                      fetch_aggregated_dictionary_id, \
                                      insert_probes, \
                                      fetch_probes

from creole.objspace import CreoleObjSpace
from creole.config import dtdfilename
from lxml.etree import tostring
from json import dumps

import tempfile
import tarfile
import base64
from os import remove, mkdir, chmod, chown, path, makedirs

PATH_EOLE='/var/lib/eole/zephir/'

def get_servermodel_files(cursor,
                          servermodelid: int,
                          tmpdirname,
                          documenttype: str):
    ids = [row[1] for row in fetch_servermodel_applicationservices(cursor, servermodelid, True)]
    services = list(ApplicationService().collect_dependencies_id(cursor, ids))
    if not services:
        raise Exception('cannot find services for servermodelid {}'.format(servermodelid))
    if documenttype == 'file':
        doc_dir = tmpdirname
    else:
        makedirs(tmpdirname + PATH_EOLE + documenttype)
        doc_dir = tmpdirname + PATH_EOLE + documenttype
    for file_ in fetch_applicationservices_documents(cursor, services, documenttype):
        try:
            if file_['documentpath'] is not None:
                if not path.exists(f'{doc_dir}{file_["documentpath"]}'):
                    makedirs(f'{doc_dir}{file_["documentpath"]}')
                filename = f'{file_["documentpath"]}/{file_[0]}'
            else:
                filename = f'/{file_[0]}'
            filepath = f'{doc_dir}{filename}'
            print(f'{filepath}')
            with open(filepath, 'wb+') as file_fh:
                file_fh.write(file_[1].tobytes())
                chmod(filepath, int(file_['documentmode'],8))

        except Exception as err:
            print(str(err))
    return doc_dir


def get_servermodel_all_files(cursor,
                            servermodelid: int):

    types =['template',
            'preservice',
            'postservice',
            'pretemplate',
            'posttemplate',
            'file']

    filename = tempfile.NamedTemporaryFile(suffix='.tar.gz')

    with tempfile.TemporaryDirectory() as tmpdirname:
        for filetype in types:
            get_servermodel_files(cursor, servermodelid, tmpdirname, filetype)

        tar = tarfile.open(filename.name, 'w:gz')
        tar.add(tmpdirname, arcname='creole_files')
        tar.close()

    with open(filename.name, 'rb') as tar_fh:
        result = base64.b64encode(tar_fh.read())
        return result

def get_servermodel_aggregated_files(cursor,
                                     servermodelid: int,
                                     documenttype: str):
    ids = [row[1] for row in fetch_servermodel_applicationservices(cursor, servermodelid, True)]

    services = list(ApplicationService().collect_dependencies_id(cursor, ids))
    if not services:
        raise Exception('cannot find services for servermodelid {}'.format(servermodelid))
    file_content = '# -*- coding: utf-8 -*-\n'
    for file_ in fetch_applicationservices_documents(cursor, services, documenttype):
        try:
            print('import {}'.format(file_[0]))
            file_content += '# {}\n'.format(file_[0])
            file_content += file_[1].tobytes().decode() + '\n'
        except Exception as err:
            import traceback
            traceback.print_exc()
            raise Exception('error in file {}: {}'.format(file_[0], str(err)))
    return file_content


def aggregate_dictionaries(cursor,
                           servermodelid: int,
                           force: bool=False):
    eolobj = CreoleObjSpace(dtdfilename)
    # assume that the first one is own applicationservice
    applicationserviceid = fetch_servermodel_applicationservice(cursor, servermodelid)
    documentid = fetch_aggregated_dictionary_id(cursor, applicationserviceid)
    if documentid is not None and not force:
        probesid = fetch_probes(cursor, applicationserviceid)
        return documentid, probesid

    ids = [row[1] for row in fetch_servermodel_applicationservices(cursor, servermodelid, True)]

    services = list(ApplicationService().collect_dependencies_id(cursor, ids))
    if not services:
        raise Exception('cannot find services for servermodelid {}'.format(servermodelid))
    for xmlfile in fetch_applicationservices_documents(cursor, services, 'dico'):
        try:
            print('import {}'.format(xmlfile[0]))
            if xmlfile[0].startswith('dictionaries/'):
                namespace = 'creole'
            else:
                namespace = xmlfile[0].split('/')[1]
            eolobj.populate_from_zephir(namespace, xmlfile[1].tobytes())
        except Exception as err:
            raise Exception('error in file {}: {}'.format(xmlfile[0], str(err)))
    eosfunc = get_servermodel_aggregated_files(cursor, servermodelid, 'creolefuncs')
    filename = tempfile.NamedTemporaryFile().name
    with open(filename, 'w') as eosfunc_fh:
        eosfunc_fh.write(eosfunc)
    eolobj.space_visitor(filename)
    remove(filename)
    xmlroot = eolobj.save(None, True)
    document = tostring(xmlroot, pretty_print=True, encoding='UTF-8')  # , xml_declaration=True)
    document_probes = dumps(eolobj.save_probes(None, True))
    if documentid is not None:
        update_document(cursor, documentid, document)
        probesid = fetch_probes(cursor, applicationserviceid)
        update_document(cursor, probesid, document_probes.encode('utf8'))
    else:
        probesid = insert_probes(cursor,
                                 'probes.json',
                                 applicationserviceid,
                                 document_probes.encode())
        documentid = insert_aggregated_dictionary(cursor,
                                                  'aggregated.xml',
                                                  applicationserviceid,
                                                  document)
    return documentid, probesid
