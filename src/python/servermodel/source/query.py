
"""
"""
from zephir.i18n import _
from ..servermodel.query import erase_servermodel
from ..applicationservice.query import erase_applicationservice
from zephir.config import DEBUG


SOURCES_QUERY = """SELECT sourceid, sourcename, sourceurl, sourceshasum FROM source;"""
SOURCE_QUERY = """SELECT sourceid, sourcename, sourceurl, sourceshasum FROM source WHERE sourcename=%s;"""
INSERTSOURCE_QUERY = """INSERT INTO source (sourcename, sourceurl) VALUES (%s, %s) RETURNING sourceid, sourcename, sourceurl, sourceshasum"""
SOURCE_DELETE = """DELETE from source"""
SHASUM_UPDATE = '''
    UPDATE source
    SET sourceshasum = %s
    WHERE sourceid = %s
    ;
'''

def fetch_source_list(cursor) -> list:
    """
    """
    cursor.execute(SOURCES_QUERY)
    fetched = cursor.fetchall()
    return fetched


def get_source(cursor, source_name: str) -> int:
    """

    """
    cursor.execute(SOURCE_QUERY, (source_name,))
    return cursor.fetchone()


def update_shasum(cursor, source_id: int, shasum: str) -> None:
    cursor.execute(SHASUM_UPDATE, (shasum, source_id))


def create_source(cursor, source_name: str, source_url: str) -> int:
    """
    Insert a new Source

    :param source:{'SourceName': 'EOLE', 'SourceURL': 'https://pcll.ac-dijon.fr/eole/'}

    """
    source = get_source(cursor, source_name)
    if source is None:
        if DEBUG:
            print(_("Insert source {} ({})").format(source_name, source_url))
        cursor.execute(INSERTSOURCE_QUERY, (source_name, source_url))
        source = cursor.fetchone()
    elif DEBUG:
        print (_("The source {} already exists").format(source_name))
    return source


def erase_source(cursor):
    erase_servermodel(cursor)
    erase_applicationservice(cursor)
    cursor.execute(SOURCE_DELETE)
