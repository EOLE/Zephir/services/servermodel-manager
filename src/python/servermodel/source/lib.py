from .query import (fetch_source_list,
                    get_source,
                    create_source,
                    erase_source,
                    update_shasum)

def source_row2obj(row):
    """
    Transforme un 'row' représentant une entrée de la table source en objet Source
    prêt à être envoyé dans une réponse WAMP
    """
    return {
        "sourceid": row[0],
        "sourcename": row[1],
        "sourceurl": row[2]
    }


class Source():

    def list_source(self, cursor):
        return [source_row2obj(r) for r in fetch_source_list(cursor)]

    def create_source(self, cursor, source_name, source_url):
        return create_source(cursor, source_name, source_url)

    def get_source(self, cursor, source_name):
        return get_source(cursor, source_name)

    def erase_source(self, cursor):
        erase_source(cursor)

    def update_shasum(self, cursor, sourceid, shasum):
        update_shasum(cursor, sourceid, shasum)
