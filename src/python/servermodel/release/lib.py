from .query import (list_release,
                    list_version,
                    list_subrelease,
                    describe_release,
                    describe_subrelease,
                    erase_release,
                    erase_subrelease,
                    erase_version,
                    create_release,
                    create_subrelease,
                    create_version,
                    get_releaseid,
                    get_subreleaseid,
                    get_versionid
                    )


def subrelease_row2obj(row):
    """
    Transforme un 'row' représentant une entrée de la table release
    prêt à être envoyé dans une réponse WAMP
    """
    return {
        "subreleaseid": row[0],
        "subreleasename": row[2],
        "subreleasereleasedid": row[1]
    }


def release_row2obj(row):
    """
    Transforme un 'row' représentant une entrée de la table release
    prêt à être envoyé dans une réponse WAMP
    """
    return {
        "releaseid": row[0],
        "releasename": row[2],
        "releaseversionid": row[1]
    }


def version_row2obj(row):
    """
    Transforme un 'row' représentant une entrée de la table version
    prêt à être envoyé dans une réponse WAMP
    """
    return {
        "versionid": row[0],
        "versionname": row[1],
        "versiondistribution": row[2],
        "versionlabel": row[3]
    }


def describe_release_row2obj(row):
    """
    Transforme un 'row' représentant la description d'une release
    prêt à être envoyé dans une réponse WAMP
    """
    return {
        "releaseid": row[0],
        "releasename": row[1],
        "versionname": row[2],
        "versiondistribution": row[3],
        "versionlabel": row[4]
    }


def describe_subrelease_row2obj(row):
    """
    Transforme un 'row' représentant la description d'une subrelease
    prêt à être envoyé dans une réponse WAMP
    """
    return {
        "subreleaseid": row[0],
        "subreleasename": row[1],
        "releasename": row[2],
        "versionname": row[3],
        "versiondistribution": row[4],
        "versionlabel": row[5]
    }


class SubRelease():

    def list_subrelease(self, cursor, release_id=None):
        return [subrelease_row2obj(r) for r in list_subrelease(cursor, release_id)]

    def get_subreleaseid(self, cursor, release_id, subrelease_name):
        return get_subreleaseid(cursor, release_id, subrelease_name)

    def describe_subrelease(self, cursor, subreleaseid):
        return describe_subrelease_row2obj(describe_subrelease(cursor, subreleaseid))

    def create_subrelease(self, cursor, release_id, subrelease_name):
        return create_subrelease(cursor, release_id, subrelease_name)

    def erase_subrelease(self, cursor):
        erase_subrelease(cursor)


class Release():

    def list_release(self, cursor):
        return [release_row2obj(r) for r in list_release(cursor)]

    def get_releaseid(self, cursor, version_id, release_name):
        return get_releaseid(cursor, version_id, release_name)

    def describe_release(self, cursor, releaseid):
        return describe_release_row2obj(describe_release(cursor, releaseid))

    def create_release(self, cursor, version_id, release_name):
        return create_release(cursor, version_id, release_name)

    def erase_release(self, cursor):
        erase_release(cursor)


class Version():

    def list_version(self, cursor):
        return [version_row2obj(r) for r in list_version(cursor)]

    def get_versionid(self, cursor, version_name, version_distribution):
        return get_versionid(cursor, version_name, version_distribution)

    def create_version(self, cursor, version_name, version_distribution, version_label):
        return create_version(cursor, version_name, version_distribution, version_label)

    def erase_version(self, cursor):
        return erase_version(cursor)
