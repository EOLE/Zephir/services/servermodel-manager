"""
"""
from zephir.i18n import _
from psycopg2 import IntegrityError
from ..applicationservice.query import erase_applicationservice
from ..servermodel.query import erase_servermodel

RELEASEID_QUERY = """SELECT releaseid FROM release WHERE releasename=%s AND releaseversionid=%s;"""
SUBRELEASECOUNT_QUERY = """SELECT COUNT(*) FROM subrelease WHERE subreleasereleaseid=%s;"""
SUBRELEASEID_QUERY = """SELECT subreleaseid FROM subrelease WHERE subreleasename=%s AND subreleasereleaseid=%s;"""
INSERTVERSION_QUERY = """INSERT INTO version (versionname, versiondistribution, versionlabel) VALUES (%s, %s, %s) RETURNING versionid;"""
INSERTRELEASE_QUERY = """INSERT INTO release (releasename, releaseversionid) VALUES (%s, %s) RETURNING releaseid;"""
INSERTSUBRELEASE_QUERY = """INSERT INTO subrelease (subreleasename, subreleasereleaseid) VALUES (%s, %s) RETURNING subreleaseid;"""
VERSIONID_QUERY = """SELECT versionid FROM version WHERE versionname=%s AND versiondistribution=%s;"""
VERSION_DELETE = """DELETE from version"""
RELEASE_DELETE = """DELETE from release"""
SUBRELEASE_DELETE = """DELETE from subrelease"""
RELEASE_QUERY = """SELECT releaseid, releaseversionid, releasename from release"""
SUBRELEASE_QUERY = """SELECT subreleaseid, subreleasereleaseid, subreleasename from subrelease"""
SUBRELEASE_BY_RELEASE_QUERY = """SELECT subreleaseid, subreleasereleaseid, subreleasename FROM subrelease WHERE subreleasereleaseid = %s"""
VERSION_QUERY = """SELECT versionid, versionname, versiondistribution, versionlabel from version"""
RELEASE_DESCRIBE_QUERY = """
    SELECT
        releaseid,
        releasename,
        versionname,
        versiondistribution,
        versionlabel
    FROM release
    JOIN version ON versionid = releaseversionid
    WHERE releaseid=%s
"""
SUBRELEASE_DESCRIBE_QUERY = """
    SELECT
        subreleaseid,
        subreleasename,
        releasename,
        versionname,
        versiondistribution,
        versionlabel
    FROM release
    JOIN version ON versionid = releaseversionid
    JOIN release ON releaseid = subreleasereleaseid
    WHERE subreleaseid=%s
"""


def list_subrelease(cursor, release_id):
    """
    """
    if release_id is None:
        cursor.execute(SUBRELEASE_QUERY)
    else:
        cursor.execute(SUBRELEASE_BY_RELEASE_QUERY, (release_id,))
    return cursor.fetchall()


def list_release(cursor):
    """
    """
    cursor.execute(RELEASE_QUERY)
    return cursor.fetchall()


def list_version(cursor):
    """
    """
    cursor.execute(VERSION_QUERY)
    return cursor.fetchall()


def describe_release(cursor, releaseid):
    """
    """
    cursor.execute(RELEASE_DESCRIBE_QUERY, (releaseid,))
    fetched = cursor.fetchone()
    if fetched is None:
        return None
    return fetched


def describe_subrelease(cursor, subreleaseid):
    """
    """
    cursor.execute(SUBRELEASE_DESCRIBE_QUERY, (subreleaseid,))
    fetched = cursor.fetchone()
    if fetched is None:
        return None
    return fetched


def get_versionid(cursor, version_name: str, version_distribution: str) -> int:
    """
    """
    cursor.execute(VERSIONID_QUERY, (version_name, version_distribution))
    fetched = cursor.fetchone()
    if fetched is None:
        return None
    return fetched[0]


def get_releaseid(cursor, version_id: int, release_name: str) -> int:
    """
    :param relase_name: str
    """
    cursor.execute(RELEASEID_QUERY, (release_name, version_id))
    fetched = cursor.fetchone()
    if fetched is None:
        return None
    return fetched[0]


def get_subreleaseid(cursor, release_id: int, subrelease_name: str) -> int:
    """
    :param relase_name: str
    """
    cursor.execute(SUBRELEASEID_QUERY, (subrelease_name, release_id))
    fetched = cursor.fetchone()
    if fetched is None:
        return None
    return fetched[0]


def create_version(cursor, version_name: str, version_distribution: str, version_label: str) -> int:
    """Creates the version

    :param cursor: psychopg connexion postgresql object
    """
    version_id = get_versionid(cursor, version_name, version_distribution)
    if version_id is None:
        print(_("Insert version {}").format(version_label))
        cursor.execute(INSERTVERSION_QUERY, (version_name, version_distribution, version_label))
        version_id = cursor.fetchone()[0]
    else:
        print(_("The version {} already exists").format(version_name))
    return version_id


def create_release(cursor, version_id: int, release_name: str) -> int:
    """
    Insert a NEW version and the related releases

    """
    release_id = get_releaseid(cursor, version_id, release_name)
    if release_id is None:
        print(_("Insert release {}").format(release_name))
        cursor.execute(INSERTRELEASE_QUERY, (release_name, version_id))
        release_id = cursor.fetchone()[0]
    else:
        print(_("The release {} already exists").format(release_name))

    return release_id

def get_counter(cursor, release_id: int) -> int:
    cursor.execute(SUBRELEASECOUNT_QUERY, (release_id,))
    return cursor.fetchone()[0]

def create_subrelease(cursor, release_id: int, subrelease_basename: str) -> int:
    """
    Insert a NEW subrelease and the related release

    """
    cnt = get_counter(cursor, release_id)
    subrelease_name = subrelease_basename + '.' + str(cnt)
    cursor.execute(INSERTSUBRELEASE_QUERY, (subrelease_name, release_id))
    return cursor.fetchone()[0]


def erase_subrelease(cursor):
    erase_applicationservice(cursor)
    erase_servermodel(cursor)
    cursor.execute(SUBRELEASE_DELETE)


def erase_release(cursor):
    erase_subrelease(cursor)
    cursor.execute(RELEASE_DELETE)


def erase_version(cursor):
    erase_release(cursor)
    cursor.execute(VERSION_DELETE)
