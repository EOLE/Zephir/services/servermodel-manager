class LoadDependenciesError(Exception):
    pass


class DependencyError(Exception):
    pass
