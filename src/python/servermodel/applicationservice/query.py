from psycopg2 import Binary

from zephir.i18n import _
from .error import LoadDependenciesError, DependencyError
from ..document.query import erase_document


APPLICATIONSERVICE_QUERY = """
SELECT applicationserviceid,
         applicationservicename,
         applicationservicedescription,
         applicationservicesubreleaseid,
         applicationservicedependencies
FROM applicationservice
WHERE applicationserviceid=%s"""

APPLICATIONSERVICEID_QUERY = """
SELECT applicationserviceid
FROM applicationservice
WHERE applicationservicename=%s
    AND applicationservicesubreleaseid=%s"""

APPLICATIONSERVICES_QUERY = """
SELECT *
FROM applicationservice"""

APPLICATIONSERVICES_BY_RELEASEID_QUERY = """
SELECT *
FROM applicationservice
WHERE applicationservicesubreleaseid=%s"""

APPLICATIONSERVICE_INSERT = """
INSERT INTO applicationservice (applicationservicename,
                                applicationservicedescription,
                                applicationservicesubreleaseid,
                                applicationservicesourceid)
VALUES (%s, %s, %s, %s)
RETURNING applicationserviceid
"""

PACKAGE_INSERT = """
INSERT INTO package (packagename, packageapplicationserviceid)
VALUES (%s, %s)
"""

APPLICATIONSERVICE_DEPENDENCIES_UPDATE = """
UPDATE applicationservice
SET applicationservicedependencies = %s
WHERE applicationserviceid = %s
RETURNING applicationserviceid"""

APPLICATIONSERVICE_DELETE = """DELETE FROM applicationservice"""

SERVERMODELAPPLICATIONSERVICE_DELETE = """DELETE FROM servermodelapplicationservice"""

DOCUMENT_INSERT = '''
INSERT INTO document (documentserviceid, documentname, documenttype, documentcontent, documentshasum)
VALUES (%s, %s, %s, %s, %s)
RETURNING documentid
'''

DOCUMENT_UPDATE = '''
UPDATE document SET documentcontent = %s, documentshasum = %s
WHERE documentid = %s
'''

DOCUMENT_DELETE = """DELETE FROM document WHERE documentid = %s"""

DOCUMENT_UPDATE_FILE = '''
UPDATE document SET documentcontent = %s, documentshasum = %s, documentowner = %s, documentgroup = %s, documentmode = %s
WHERE documentid = %s
'''

DOCUMENT_INSERT_SCRIPT = '''
INSERT INTO document (documentserviceid, documentname, documenttype, documentcontent, documentmode, documentshasum)
VALUES (%s, %s, %s, %s, '0755', %s)
'''


DOCUMENT_INSERT_FILE = '''
INSERT INTO document (documentserviceid, documentname, documenttype, documentcontent, documentpath, documentowner, documentgroup, documentmode, documentshasum)
VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)
'''

ARCHIVE_FILE = '''
INSERT INTO document (documentname, documenttype, documentcontent, documentpath, documentowner, documentgroup, documentmode, documentshasum)
SELECT documentname, documenttype, documentcontent, documentpath, documentowner, documentgroup, documentmode, documentshasum
FROM document
WHERE documentid = %s
'''

FETCH_DOCUMENT = '''
    SELECT documentcontent
    FROM document
    WHERE documentid = %s;
'''

FETCH_DOCUMENT_ID = '''
    SELECT documentid
    FROM document
    WHERE documentserviceid = %s AND documenttype = %s
'''


def fetchone(cursor, query: str, query_parameters: tuple=None, raises: bool=False):
    cursor.execute(query, query_parameters)
    fetched = cursor.fetchone()
    if not fetched:
        if raises:
            raise DependencyError(_("cannot find element with query {} and parameters {}").format(query, query_parameters))
        fetched = None
    return fetched


def list_services_by_subrelease(cursor, subreleaseid: int):
    """Fetch all services for given sub-release id
    """
    cursor.execute(APPLICATIONSERVICES_BY_RELEASEID_QUERY, (subreleaseid,))
    fetched = cursor.fetchall()
    return fetched


def list_services(cursor):
    """Fetch all services
    """
    cursor.execute(APPLICATIONSERVICES_QUERY)
    fetched = cursor.fetchall()
    return fetched


def fetch_document(cursor, documentid: int):
    return fetchone(cursor, FETCH_DOCUMENT, (documentid,))[0].tobytes()


def fetch_aggregated_dictionary_id(cursor, applicationserviceid):
    documentid = fetchone(cursor, FETCH_DOCUMENT_ID, (applicationserviceid, 'aggregated_dico'))
    if documentid is None:
        return documentid
    return documentid[0]


def fetch_probes(cursor,
                 applicationserviceid):
    documentid = fetchone(cursor,
                          FETCH_DOCUMENT_ID,
                          (applicationserviceid, 'probes'))
    if documentid is None:
        return documentid
    return documentid[0]


def fetch_applicationservice_by_id(cursor, applicationserviceid: int):
    """Fetch application service
    """
    applicationservice = fetchone(cursor, APPLICATIONSERVICE_QUERY, (applicationserviceid,))
    return applicationservice


def fetch_applicationserviceid(cursor, name: str,
                               subreleaseid: int):
    """Fetch application services for given application service name and sub-release id
    """
    applicationserviceid = fetchone(cursor, APPLICATIONSERVICEID_QUERY, (name, subreleaseid))
    if applicationserviceid is None:
        return applicationserviceid
    return applicationserviceid['applicationserviceid']


def insert_applicationservice(cursor, name, description, subreleaseid, sourceid) -> int:
    """Insert application service into database
    """
    cursor.execute(APPLICATIONSERVICE_INSERT, (name, description, subreleaseid, sourceid))
    applicationserviceid = cursor.fetchone()
    if applicationserviceid is None:
        return applicationserviceid
    return applicationserviceid['applicationserviceid']


def update_dependencies(cursor, applicationserviceid: int, depends):
    """Update application service entry adding dependencies
    """
    applicationservice = fetchone(cursor, APPLICATIONSERVICE_DEPENDENCIES_UPDATE, (depends, applicationserviceid))
    return applicationservice


def erase_servermodelapplicationservice(cursor):
    cursor.execute(SERVERMODELAPPLICATIONSERVICE_DELETE)


def erase_applicationservice(cursor):
    erase_document(cursor)
    erase_servermodelapplicationservice(cursor)
    cursor.execute(APPLICATIONSERVICE_DELETE)


def insert_dictionary(cursor, name: str, applicationserviceid: int, content, checksum) -> None:
    cursor.execute(DOCUMENT_INSERT, (applicationserviceid, name, 'dico', Binary(content), checksum))
    return cursor.fetchone()[0]


def insert_aggregated_dictionary(cursor, name: str, applicationserviceid: int, content) -> int:
    cursor.execute(DOCUMENT_INSERT, (applicationserviceid, name, 'aggregated_dico', Binary(content), ''))
    return cursor.fetchone()[0]


def insert_probes(cursor,
                  name: str,
                  applicationserviceid: int,
                  content) -> int:
    cursor.execute(DOCUMENT_INSERT,
                   (applicationserviceid, name, 'probes', Binary(content), ''))
    return cursor.fetchone()[0]


def update_document(cursor, documentid: int, content: bytes, documentshasum: str=None) -> None:
    cursor.execute(DOCUMENT_UPDATE, (Binary(content), documentshasum, documentid))

def delete_document(cursor, documentid: int) -> None:
    cursor.execute(DOCUMENT_DELETE, (documentid,))

def update_document_file(cursor, documentid: int, content: bytes, documentshasum: str, owner: str, group: str, mode: str) -> None:
    cursor.execute(DOCUMENT_UPDATE_FILE, (Binary(content), documentshasum, owner, group, mode, documentid))


def insert_template(cursor, name: str, applicationserviceid: int, content, checksum) -> None:
    cursor.execute(DOCUMENT_INSERT, (applicationserviceid, name, 'template', Binary(content), checksum))


def insert_pretemplate(cursor, name: str, applicationserviceid: int, content, checksum) -> None:
    cursor.execute(DOCUMENT_INSERT_SCRIPT, (applicationserviceid, name, 'pretemplate', Binary(content), checksum))


def insert_posttemplate(cursor, name: str, applicationserviceid: int, content, checksum) -> None:
    cursor.execute(DOCUMENT_INSERT_SCRIPT, (applicationserviceid, name, 'posttemplate', Binary(content), checksum))


def insert_preservice(cursor, name: str, applicationserviceid: int, content, checksum) -> None:
    cursor.execute(DOCUMENT_INSERT_SCRIPT, (applicationserviceid, name, 'preservice', Binary(content), checksum))


def insert_postservice(cursor, name: str, applicationserviceid: int, content, checksum) -> None:
    cursor.execute(DOCUMENT_INSERT_SCRIPT, (applicationserviceid, name, 'postservice', Binary(content), checksum))


def insert_creolefunc(cursor, name: str, applicationserviceid: int, content, checksum) -> None:
    cursor.execute(DOCUMENT_INSERT, (applicationserviceid, name, 'creolefuncs', Binary(content), checksum))


def insert_file(cursor, name: str, applicationserviceid: int, content, dest, owner, group, mode, checksum) -> None:
    cursor.execute(DOCUMENT_INSERT_FILE, (applicationserviceid, name, 'file', Binary(content), dest, owner, group, mode, checksum))


def archive_document(cursor, documentid):
    cursor.execute(ARCHIVE_FILE, (documentid,))
