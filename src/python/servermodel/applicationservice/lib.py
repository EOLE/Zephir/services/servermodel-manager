from json import loads, dumps
import yaml
import re
from os.path import dirname, join, split

from zephir.i18n import _
from .error import LoadDependenciesError, DependencyError
from .query import (list_services,
                    list_services_by_subrelease,
                    fetch_applicationserviceid,
                    erase_applicationservice,
                    erase_servermodelapplicationservice,
                    insert_applicationservice,
                    update_dependencies,
                    insert_dictionary,
                    insert_template,
                    insert_pretemplate,
                    insert_posttemplate,
                    insert_preservice,
                    insert_postservice,
                    insert_creolefunc,
                    insert_file,
                    fetch_document,
                    update_document,
                    update_document_file,
                    archive_document,
                    fetch_applicationservice_by_id)
from ..servermodel.query import fetch_information_applicationservice_document
from ..release.query import get_releaseid


FILE_MODE_RE = re.compile(r"mode: (0[0-9]{3})")
SERVICE_KEYS = set(['name', 'description', "depends"])
ALLOWED_SERVICE_CHAR = re.compile(r"^[a-z0-9-]*$")

class ApplicationService():
    """
    Fetch application services description from YAML files.
    Populate database.
    """
    def __init__(self):
        self.services = []
        self.depends = []
        self.provides = []
        self.updated_applicationservices = set()

    def process_yaml(self, cursor, release, subreleaseid, yaml_content, yaml_path):
        """
        Process yaml file
        """
        try:
            # Parsing 1.2 yaml with 1.1 compliant parser: protect string from octal to int conversion
            yaml_content = FILE_MODE_RE.sub(r"mode: '\1'", yaml_content)
            service = yaml.load(yaml_content)
        except yaml.scanner.ScannerError:
            raise LoadDependenciesError(_("Unable to load yaml file {}"
                                          "").format(yaml_path))
        if not isinstance(service, dict):
            raise LoadDependenciesError(_("Root YAML must be a dict, file {} is a {}"
                                          "").format(yaml_path, service.__class__.__name__))
        mandatory_keys = SERVICE_KEYS - set(service.keys())
        if mandatory_keys:
            sorted_mandatory_keys = list(mandatory_keys)
            sorted_mandatory_keys.sort()
            raise LoadDependenciesError(_('Mandatory keys: "{}" in file {}'
                                          '').format(', '.join(sorted_mandatory_keys), yaml_path))
        match = ALLOWED_SERVICE_CHAR.search(service['name'])
        if not match:
            raise LoadDependenciesError(_("Service {} contains not allowed chars in file {}"
                                          "").format(service['name'], yaml_path))
        self.depends.append({'name': service['name'],
                             'depends': service['depends'],
                             'release': release,
                             'subreleaseid': subreleaseid,
                             'yamlpath': yaml_path})
        #self.provides.append({'name': service['name'],
        #                      'provides': service['provides'],
        #                      'release': release,
        #                      'subreleaseid': subreleaseid})

        self.services.append({'name': service['name'],
                              'description': service['description'],
                              'release': release,
                              'subreleaseid': subreleaseid,
                              'files': service.get('files'),
                              'yamlpath': yaml_path})

    def insert_applicationservices(self, cursor, source_id: int) -> None:
        for service in self.services:
            fetched = fetch_applicationserviceid(cursor,
                                                 service['name'],
                                                 service['subreleaseid'])
            documents = {}
            if fetched is not None:
                print(_("ApplicationService {}-{} already exists").format(service['name'],
                                                                          service['release']))
                applicationserviceid = fetched
                for document in fetch_information_applicationservice_document(cursor, applicationserviceid):
                    documents.setdefault(document['documenttype'], {}).setdefault(document['documentpath'], {})
                    if document['documentname'] in documents[document['documenttype']][document['documentpath']]:
                        raise Exception('duplicate document {}'.format(document['documentname']))
                    documents[document['documenttype']][document['documentpath']][document['documentname']] = dict(document)

            else:
                print(_("Create ApplicationService {}-{}").format(service['name'],
                                                                  service['release']))
                applicationserviceid = insert_applicationservice(cursor, service['name'],
                                                                 service['description'][:255],
                                                                 service['subreleaseid'],
                                                                 source_id)
            self.insert_dictionaries(cursor,
                                     applicationserviceid,
                                     service['yamlpath'],
                                     documents)
            self.insert_templates(cursor,
                                  applicationserviceid,
                                  service['yamlpath'],
                                  documents)
            self.insert_pretemplates(cursor,
                                     applicationserviceid,
                                     service['yamlpath'],
                                     documents)
            self.insert_posttemplates(cursor,
                                      applicationserviceid,
                                      service['yamlpath'],
                                      documents)
            self.insert_preservices(cursor,
                                    applicationserviceid,
                                    service['yamlpath'],
                                    documents)
            self.insert_postservices(cursor,
                                     applicationserviceid,
                                     service['yamlpath'],
                                     documents)
            self.insert_creolefuncs(cursor,
                                    applicationserviceid,
                                    service['yamlpath'],
                                    documents)
            if service.get('files'):
                files = service['files'].items()
                self.insert_files(cursor,
                                  applicationserviceid,
                                  service['yamlpath'],
                                  files,
                                  documents.get('file', {}))

    def insert_dependencies(self, cursor):
        for depend in self.depends:
            serviceids = []
            for service in depend['depends']:
                if isinstance(service, str):
                    serviceids.append(self.get_id_by_service(cursor,
                                                             service,
                                                             depend['subreleaseid'],
                                                             depend['name']))
                elif isinstance(service, dict):
                    keys = set(service.keys())
                    if keys != set(['or']):
                        raise LoadDependenciesError(_("Unknown dependencies format: {}"
                                                      "").format(depend['depends']))
                    or_ids = []
                    for or_service in service['or']:
                        or_ids.append(self.get_id_by_service(cursor,
                                                             or_service,
                                                             depend['subreleaseid'],
                                                             depend['name']
                                                             ))
                    serviceids.append({'or': or_ids})
                else:
                    raise LoadDependenciesError(_("Unknown dependencies format: {}"
                                                  "").format(depend['depends']))


            depends = dumps(serviceids)
            applicationserviceid = fetch_applicationserviceid(cursor, depend['name'], depend['subreleaseid'])
            applicationservice = update_dependencies(cursor, applicationserviceid, depends)

    def insert_dictionaries(self,
                            cursor,
                            applicationserviceid: int,
                            yamlpath: str,
                            documents: dict) -> None:
        dictionaries_path = yamlpath.rsplit('/', 1)[0] + '/dictionaries'
        for dictionary_path, dictionary, checksum, old_documentid in self.get_dictionaries(dictionaries_path, documents.get('dico', {}).get(None, {})):
            self.updated_applicationservices.add(applicationserviceid)
            if old_documentid is None:
                insert_dictionary(cursor, dictionary_path, applicationserviceid, dictionary, checksum)
            else:
                archive_document(cursor, old_documentid)
                update_document(cursor, old_documentid, dictionary, documentshasum=checksum)
        dictionaries_path = yamlpath.rsplit('/', 1)[0] + '/extra_dictionaries'
        for dictionary_path, dictionary, checksum, old_documentid in self.get_dictionaries(dictionaries_path, documents.get('dico', {}).get(None, {}), extra=True):
            self.updated_applicationservices.add(applicationserviceid)
            if old_documentid is None:
                insert_dictionary(cursor, dictionary_path, applicationserviceid, dictionary, checksum)
            else:
                archive_document(cursor, old_documentid)
                update_document(cursor, old_documentid, dictionary, documentshasum=checksum)


    def insert_templates(self, cursor, applicationserviceid: int, yamlpath: str, documents: dict) -> None:
        template_type = 'template'
        templates_path = yamlpath.rsplit('/', 1)[0] + '/' + template_type
        for template_path, template, checksum, old_documentid in self.get_templates(templates_path, documents.get(template_type, {}).get(None, {})):
            self.updated_applicationservices.add(applicationserviceid)
            if old_documentid is None:
                self.insert_template(cursor, template_path.split('/')[-1], applicationserviceid, template, checksum)
            else:
                archive_document(cursor, old_documentid)
                update_document(cursor, old_documentid, template, documentshasum=checksum)

    def insert_pretemplates(self, cursor, applicationserviceid: int, yamlpath: str, documents: dict) -> None:
        template_type = 'pretemplate'
        templates_path = yamlpath.rsplit('/', 1)[0] + '/' + template_type
        for template_path, template, checksum, old_documentid in self.get_templates(templates_path, documents.get(template_type, {}).get(None, {})):
            self.updated_applicationservices.add(applicationserviceid)
            if old_documentid is None:
                self.insert_pretemplate(cursor, template_path.split('/')[-1], applicationserviceid, template, checksum)
            else:
                archive_document(cursor, old_documentid)
                update_document(cursor, old_documentid, template, documentshasum=checksum)

    def insert_posttemplates(self, cursor, applicationserviceid: int, yamlpath: str, documents: dict) -> None:
        template_type = 'posttemplate'
        templates_path = yamlpath.rsplit('/', 1)[0] + '/' + template_type
        for template_path, template, checksum, old_documentid in self.get_templates(templates_path, documents.get(template_type, {}).get(None, {})):
            self.updated_applicationservices.add(applicationserviceid)
            if old_documentid is None:
                self.insert_posttemplate(cursor, template_path.split('/')[-1], applicationserviceid, template, checksum)
            else:
                archive_document(cursor, old_documentid)
                update_document(cursor, old_documentid, template, documentshasum=checksum)

    def insert_preservices(self, cursor, applicationserviceid: int, yamlpath: str, documents: dict) -> None:
        template_type = 'preservice'
        templates_path = yamlpath.rsplit('/', 1)[0] + '/' + template_type
        for template_path, template, checksum, old_documentid in self.get_templates(templates_path, documents.get(template_type, {}).get(None, {})):
            self.updated_applicationservices.add(applicationserviceid)
            if old_documentid is None:
                self.insert_preservice(cursor, template_path.split('/')[-1], applicationserviceid, template, checksum)
            else:
                archive_document(cursor, old_documentid)
                update_document(cursor, old_documentid, template, documentshasum=checksum)

    def insert_postservices(self, cursor, applicationserviceid: int, yamlpath: str, documents: dict) -> None:
        template_type = 'postservice'
        templates_path = yamlpath.rsplit('/', 1)[0] + '/' + template_type
        for template_path, template, checksum, old_documentid in self.get_templates(templates_path, documents.get(template_type, {}).get(None, {})):
            self.updated_applicationservices.add(applicationserviceid)
            if old_documentid is None:
                self.insert_postservice(cursor, template_path.split('/')[-1], applicationserviceid, template, checksum)
            else:
                archive_document(cursor, old_documentid)
                update_document(cursor, old_documentid, template, documentshasum=checksum)

    def insert_creolefuncs(self, cursor, applicationserviceid: int, yamlpath: str, documents: dict) -> None:
        template_type = 'creolefuncs'
        templates_path = yamlpath.rsplit('/', 1)[0] + '/creole_funcs'
        for template_path, template, checksum, old_documentid in self.get_templates(templates_path, documents.get(template_type, {}).get(None, {})):
            self.updated_applicationservices.add(applicationserviceid)
            if old_documentid is None:
                self.insert_creolefunc(cursor, template_path.split('/')[-1], applicationserviceid, template, checksum)
            else:
                archive_document(cursor, old_documentid)
                update_document(cursor, old_documentid, template, documentshasum=checksum)

    def insert_files(self, cursor, applicationserviceid: int, yamlpath: str, files: list, documents) -> None:
        for file_path, perm in files:
            dest, file_name = split(file_path)
            elements = self.get_file(yamlpath.rsplit('/', 1)[0] + '/files' + file_path)
            if elements:
                path, content, checksum = elements
                owner = perm.get('owner', 'root')
                group = perm.get('group', 'root')
                mode = perm.get('mode', '0644')
                if dest in documents and file_name in documents[dest] and checksum in documents[dest][file_name]['documentshasum']:
                    if checksum in documents[dest][file_name]['documentshasum'] and \
                            documents[dest][file_name]['documentowner'] == owner and \
                            documents[dest][file_name]['documentgroup'] == group and \
                            documents[dest][file_name]['documentmode'] == mode:
                        continue
                    old_documentid = documents[dest][file_name]['documentid']
                    archive_document(cursor, old_documentid)
                    update_document_file(cursor, old_documentid, content, checksum, owner, group, mode)
                else:
                    self.insert_file(cursor, file_name, applicationserviceid, content, dest, owner, group, mode, checksum)
                self.updated_applicationservices.add(applicationserviceid)

    def insert_provides(self, cursor):
        pass

    def get_service_by_id(self, cursor, serviceid: int) -> str:
        applicationservice = fetch_applicationservice_by_id(cursor, serviceid)
        if applicationservice is None:
            raise DependencyError(_("Unknown serviceid {}").format(serviceid))
        return applicationservice

    def get_id_by_service(self, cursor, service: str, subreleaseid: int,  origin_service: str=None) -> int:
        applicationserviceid = fetch_applicationserviceid(cursor, service, subreleaseid)
        if applicationserviceid is None:
            if origin_service is None:
                raise LoadDependenciesError(_("non-existent service {}").format(service))
            else:
                raise LoadDependenciesError(_("{} depending on non-existent service {}"
                                              "").format(origin_service, service))
        return applicationserviceid

    def get_columns_index_by_name(self, cursor) -> dict:
        """convenience method to retrieve the columns by name
        """
        cols = {}
        for index, col in enumerate(cursor.description):
            cols[col.name] = index
        return cols

    def format_service_form_entry(self, entry: tuple, cols: dict) -> dict:
        return {'id': entry[cols['applicationserviceid']],
                'name': entry[cols['applicationservicename']],
                'description': entry[cols['applicationservicedescription']]}

    def list_services(self, cursor, subreleaseid: int):
        """Fetch all services
        """
        fetched = list_services_by_subrelease(cursor, subreleaseid)
        cols = self.get_columns_index_by_name(cursor)
        return (self.format_service_form_entry(entry, cols) for entry in fetched)

    def fetch_dependencies(self, services):
        """Fetch services dependencies.
        """
        pass

    def prospect_optional_dependencies(self, cursor, services: list, new_service: int) -> list:
        """if 'or' dependencies is present, decides if the dependency is correct or not.
        Otherwise, return the choices to the user to make a decision.
        """
        cols = {'applicationserviceid': 0,
                'applicationservicename': 1,
                'applicationservicedescription': 2,
                'applicationservicesubreleaseid': 3}
        all_services = list(self.collect_dependencies_id(cursor, services))
        or_depends = []
        self._get_service_by_depends(cursor, new_service, or_depends, all_services)
        set_services = set(all_services)

        for or_depend in or_depends:
            if not set_services & set(or_depend):
                ret = []
                for serviceid in or_depend:
                    service_db = self.get_service_by_id(cursor, serviceid)
                    ret.append(self.format_service_form_entry(service_db, cols))
                yield ret

    def _get_service_by_depends(self,
                                cursor,
                                serviceid: int,
                                or_depends: list,
                                ids: list) -> None:
        ids.append(serviceid)
        service_db = self.get_service_by_id(cursor, serviceid)
        if service_db[4] is not None:
            for depend in service_db[4]:
                if isinstance(depend, dict):
                    or_depends.append(depend['or'])
                else:
                    if depend not in ids:
                        self._get_service_by_depends(cursor, depend, or_depends, ids)
        #return ret

    def _parse_or_depends(self,
                          cursor,
                          or_depends: list,
                          ids: list) -> None:
        new_or_depends = []
        for or_depend in or_depends:
            if not set(or_depend) & set(ids):
                serviceid = or_depend[0]
                self._get_service_by_depends(cursor, serviceid, new_or_depends, ids)
        if new_or_depends:
            self._parse_or_depends(cursor, new_or_depends, ids)

    def collect_dependencies_id(self,
                                cursor,
                                services_id: list) -> None:
        or_depends = []
        ids = list()
        for serviceid in services_id:
            self._get_service_by_depends(cursor, serviceid, or_depends, ids)
        self._parse_or_depends(cursor, or_depends, ids)
        return set(ids)

    def get_dependencies(self, cursor, services_id: list) -> list:
        """Return consolidated dependencies or raise.
        """
        cols = {'applicationserviceid': 0,
                'applicationservicename': 1,
                'applicationservicedescription': 2,
                'applicationservicesubreleaseid': 3}
        for serviceid in self.collect_dependencies_id(cursor, services_id):
            service_db = self.get_service_by_id(cursor, serviceid)
            yield self.format_service_form_entry(service_db, cols)

    def erase_applicationservice(self, cursor):
        erase_servermodelapplicationservice(cursor)
        erase_applicationservice(cursor)

    def get_dictionaries(self, name: str) -> str:
        print('Pas de chargement des dictionnaires')

    def get_templates(self, src_path: str) -> str:
        print('Pas de chargement des templates')

    def get_file(self, src_path: str) -> str:
        print('Pas de chargement des documents')

    def insert_template(self, cursor, name: str, applicationserviceid: int, content, checksum) -> None:
        return insert_template(cursor, name, applicationserviceid, content, checksum)

    def insert_pretemplate(self, cursor, name: str, applicationserviceid: int, content, checksum) -> None:
        return insert_pretemplate(cursor, name, applicationserviceid, content, checksum)

    def insert_posttemplate(self, cursor, name: str, applicationserviceid: int, content, checksum) -> None:
        return insert_posttemplate(cursor, name, applicationserviceid, content, checksum)

    def insert_preservice(self, cursor, name: str, applicationserviceid: int, content, checksum) -> None:
        return insert_preservice(cursor, name, applicationserviceid, content, checksum)

    def insert_postservice(self, cursor, name: str, applicationserviceid: int, content, checksum) -> None:
        return insert_postservice(cursor, name, applicationserviceid, content, checksum)

    def insert_creolefunc(self, cursor, name: str, applicationserviceid: int, content, checksum) -> None:
        return insert_creolefunc(cursor, name, applicationserviceid, content, checksum)

    def insert_file(self, cursor, name: str, applicationserviceid: int, content, dest: str, owner: str, group: str, mode: str, checksum):
        return insert_file(cursor, name, applicationserviceid, content, dest, owner, group, mode, checksum)

    def fetch_document(self, cursor, documentid: int):
        return fetch_document(cursor, documentid)

    def fetch_applicationservice_by_id(self, cursor, applicationserviceid):
        return fetch_applicationservice_by_id(cursor, applicationserviceid)
