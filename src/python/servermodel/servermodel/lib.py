from .error import ServermodelError
from zephir.i18n import _
from zephir.config import DEBUG
from .query import (insert_servermodel,
                    fetch_servermodel_simple_by_id,
                    fetch_servermodel_list,
                    servermodel_by_data,
                    erase_servermodel,
                    fetch_servermodel_by_name,
                    join_servermodel_application,
                    fetch_servermodel_applicationservices,
                    fetch_servermodel_applicationservice,
                    fetch_servermodel_list_by_source,
                    get_applicationservices_by_servermodel,
                    fetch_servermodel_by_parent)
from ..applicationservice.query import insert_applicationservice, update_dependencies, fetch_aggregated_dictionary_id
from ..applicationservice import ApplicationService
from ..lib import aggregate_dictionaries
from json import dumps


def servermodel_row2obj(row):
    """
    Transforme un 'row' représentant une entrée de la table servermodel en objet ServerModel
    prêt à être envoyé dans une réponse WAMP
    """
    dico = {
        "servermodelid": row['servermodelid'],
        "servermodelname": row['servermodelname'],
        "servermodeldescription": row['servermodeldescription'],
        "servermodelversion": {
            "versionname": row['versionname'],
            "subreleasename": row['subreleasename'],
            "subreleaseid": row['subreleaseid'],
            "versiondistribution": row['versiondistribution']
        },
        "servermodelsource": {
            "sourcename": row['sourcename'],
            "sourceid": row['sourceid'],
            "sourceurl": row['sourceurl']
        }
    }
    if row['servermodelparentsid'] is not None:
        dico["servermodelparentsid"] = row['servermodelparentsid']
    return dico


def simple_servermodel_row2obj(row):
    """
    Transforme un 'row' représentant une entrée de la table servermodel en objet ServerModel
    prêt à être envoyé dans une réponse WAMP
    """
    dico = {
        "servermodelid": row['servermodelid'],
        "servermodelname": row['servermodelname'],
        "servermodeldescription": row['servermodeldescription'],
        "sourceid": row['servermodelsourceid'],
        "subreleaseid": row['servermodelsubreleaseid'],
        "subreleasename": row['subreleasename']
    }
    if row['servermodelparentsid'] is not None:
        dico["servermodelparentsid"] = row['servermodelparentsid']
    return dico


def applicationservice_row2obj(row):
    """
    Transforme un 'row' représentant une entrée de la table applicationservice en objet ApplicationService
    prêt à être envoyé dans une réponse WAMP
    """
    return {
        "name": row[0],
        "id": row[1],
        "description": row[2],
        #"applicationservicesource": {
        #    "sourceid": row[2],
        #    "sourcename": row[3],
        #    "sourceurl": row[4]
        #},
        #"applicationservicepackages": row[5]
    }

class Servermodel:
    def create_servermodel(self,
                           cursor,
                           servermodelparentsid,
                           servermodelname,
                           servermodeldescription,
                           subreleaseid,
                           sourceid,
                           raise_if_exists=True,
                           dependencies=None,
                           updated_applicationservices=None):
        # Tester si le sous-modèle n'existe pas déjà (nom/releaseid/sourceid)
        updated = False
        try:
            model = servermodel_by_data(cursor, servermodelname, subreleaseid, sourceid)
        except ServermodelError:
            if DEBUG:
                print(_('Create new servermodel {}').format(servermodelname))
            servermodelid = insert_servermodel(cursor,
                                               servermodelname,
                                               servermodeldescription,
                                               sourceid,
                                               servermodelparentsid,
                                               subreleaseid)[0]

            description = f'local applications service for {servermodelname}'
            applicationserviceid = insert_applicationservice(cursor,
                                                             f'local_{servermodelname}',
                                                             description,
                                                             subreleaseid,
                                                             sourceid)
            self.join_servermodel_application(cursor, servermodelid, applicationserviceid)
            # add servermodel dependencies to associate application service
            if dependencies:
                self.update_dependencies(cursor,
                                         applicationserviceid,
                                         dependencies)

            created = True
        else:
            if raise_if_exists:
                raise ServermodelError(_("The servermodel {} already exists").format(servermodelname))
            if DEBUG:
                print(_("The servermodel {} already exists").format(servermodelname))
            servermodelid = model[0]
            if updated_applicationservices:
                ids = [row['applicationserviceid'] for row in fetch_servermodel_applicationservices(cursor, servermodelid, True)]
                services = list(ApplicationService().collect_dependencies_id(cursor, ids))
                for service in services:
                    if service in updated_applicationservices:
                        applicationserviceid = fetch_servermodel_applicationservice(cursor, servermodelid)
                        documentid = fetch_aggregated_dictionary_id(cursor, applicationserviceid)
                        updated = True
            created = False
        servermodel = fetch_servermodel_simple_by_id(cursor, servermodelid)
        servermodel_dict = simple_servermodel_row2obj(servermodel)
        self.aggregate_dictionaries(cursor, servermodelid, force=True)
        if not raise_if_exists:
            return created, updated, servermodel_dict
        return servermodel_dict

    def aggregate_dictionaries(self, cursor, servermodelid, force=False):
        aggregate_dictionaries(cursor, servermodelid, force=True)

    def list_servermodel(self, cursor):
        return (simple_servermodel_row2obj(r) for r in fetch_servermodel_list(cursor))

    def update_dependencies(self,
                            cursor,
                            applicationserviceid,
                            dependencies):
        update_dependencies(cursor, applicationserviceid, dumps(dependencies))

    def list_servermodel_by_source(self, cursor, sourceid):
        return (simple_servermodel_row2obj(r) for r in fetch_servermodel_list_by_source(cursor, sourceid))

    def describe_servermodel(self, cursor, servermodelid: int):
        return simple_servermodel_row2obj(self.fetch_servermodel_simple_by_id(cursor, servermodelid))

    def erase_servermodel(self, cursor):
        erase_servermodel(cursor)

    def fetch_servermodel_by_name(self, cursor, name, sourceid, releaseid):
        return fetch_servermodel_by_name(cursor, name, sourceid, releaseid)

    def fetch_servermodel_simple_by_id(self,
                                       cursor,
                                       servermodel_id: int) -> tuple:
        return fetch_servermodel_simple_by_id(cursor,
                                              servermodel_id)

    def join_servermodel_application(self, cursor, servermodelid, applicationserviceid):
        join_servermodel_application(cursor, servermodelid, applicationserviceid)

    def fetch_servermodel_applicationservices(self, cursor, servermodelid: int, inheritance: bool=True):
        service_rows = fetch_servermodel_applicationservices(cursor, servermodelid, inheritance)
        return [applicationservice_row2obj(r) for r in service_rows]

    def fetch_servermodel_by_parent(self, cursor, parentid: int):
        return (simple_servermodel_row2obj(r) for r in  fetch_servermodel_by_parent(cursor, parentid))
