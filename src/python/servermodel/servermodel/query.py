from zephir.i18n import _
from .error import ServermodelError


"""
Récupère la liste des modèles de serveur fusionnés avec leur source/version/release
"""
SERVERMODEL_FETCH_ALL_QUERY = '''
    SELECT servermodelid, servermodelname, servermodelsourceid, servermodeldescription,
           servermodelparentsid, servermodelsubreleaseid, subreleasename
        FROM servermodel AS model
        JOIN subrelease ON model.servermodelsubreleaseid = subrelease.subreleaseid
        ORDER BY subrelease.subreleasename, servermodelname
    ;
'''
SERVERMODEL_FETCH_ALL_BY_SOURCE_QUERY = '''
    SELECT servermodelid, servermodelname, servermodelsourceid, servermodeldescription,
           servermodelparentsid, servermodelsubreleaseid, subreleasename
        FROM servermodel AS model
        JOIN subrelease ON model.servermodelsubreleaseid = subrelease.subreleaseid
        WHERE model.servermodelsourceid = %s
        ORDER BY subrelease.subreleasename, servermodelname
    ;
'''
"""
Récupère les attributs d'un modèle de serveur à partir de son id
"""
SERVERMODEL_FETCH_SERVERMODELID_BY_ID = '''
    SELECT servermodelid, servermodelname, servermodelsourceid, servermodeldescription,
           servermodelparentsid, servermodelsubreleaseid, subreleasename
        FROM servermodel AS model
        JOIN subrelease ON model.servermodelsubreleaseid = subrelease.subreleaseid
        WHERE servermodelid = %s
    ;
'''
"""
Récupère la liste des modèles de serveur selon un parent
"""
SERVERMODEL_FETCH_BY_PARENT = '''
    SELECT servermodelid, servermodelname, servermodelsourceid, servermodeldescription,
           servermodelparentsid, servermodelsubreleaseid, subreleasename
        FROM servermodel AS model
        JOIN subrelease ON model.servermodelsubreleaseid = subrelease.subreleaseid
        WHERE %s = ANY ( model.servermodelparentsid )
    ;
'''
"""
Récupère les attributs d'un modèle de serveur avec un nom, une release et une source
"""
SERVERMODEL_FETCH_SERVERMODEL_BY_DATA = '''
    SELECT servermodelid, servermodelname, servermodelsourceid, servermodeldescription,
           servermodelparentsid, servermodelsubreleaseid, subreleasename
        FROM servermodel AS model
        JOIN subrelease ON model.servermodelsubreleaseid = subrelease.subreleaseid
        WHERE servermodelname = %s
        AND model.servermodelsubreleaseid = %s
        AND servermodelsourceid = %s
    ;
'''

"""
Récupère les services applicatifs lié à un modèle de serveur donné (via son identifiant)
"""
APPLICATIONSERVICE_FETCH_SERVELMODELS_SERVICES_INHERITANCE_QUERY = '''
-- On récupère la hiérarchie de modèles dans une table temporaire
    WITH RECURSIVE servermodel_hierarchy(servermodelid, servermodelname, servermodelparentsid) AS (
      SELECT m.servermodelid, m.servermodelname, m.servermodelparentsid FROM servermodel m WHERE m.servermodelid = %s
      UNION ALL SELECT i.servermodelid, i.servermodelname, i.servermodelparentsid
        FROM servermodel i, servermodel_hierarchy h
        WHERE i.servermodelid = ANY ( h.servermodelparentsid )
    )
-- On récupère tous les services applicatifs liés à la hiérarchie
    SELECT appser.applicationservicename, appser.applicationserviceid, appser.applicationservicedescription
        FROM applicationservice AS appser
        JOIN servermodelapplicationservice AS smas ON appser.applicationserviceid = smas.applicationserviceid
    WHERE smas.servermodelid = ANY (SELECT servermodelid FROM servermodel_hierarchy h) ORDER BY smas.servermodelapplicationserviceid ASC
'''

DOCUMENT_FROM_APPLICATIONSERVICES = '''
    SELECT doc.documentname, documentcontent, documentpath, documentowner, documentgroup, documentmode FROM document doc
    WHERE doc.documenttype = %s AND doc.documentserviceid IN %s ORDER BY doc.documentname ASC ;
'''

DOCUMENT_SHASUM_ID_FROM_APPLICATIONSERVICE = '''
    SELECT documentid, documentname, documentpath, documentowner, documentgroup, documentmode, documenttype, documentshasum FROM document
    WHERE documentserviceid = %s;
'''

APPLICATIONSERVICE_FETCH_SERVELMODELS_SERVICES_QUERY = '''
    SELECT appser.applicationservicename, appser.applicationserviceid, appser.applicationservicedescription
        FROM applicationservice AS appser
        JOIN servermodelapplicationservice AS smas ON appser.applicationserviceid = smas.applicationserviceid
    WHERE smas.servermodelid = %s ORDER BY smas.servermodelapplicationserviceid ASC
'''

APPLICATIONSERVICE_FETCH_SERVELMODEL_QUERY = '''
    SELECT appser.applicationserviceid
        FROM applicationservice AS appser
        JOIN servermodelapplicationservice AS smas ON appser.applicationserviceid = smas.applicationserviceid
    WHERE smas.servermodelid = %s ORDER BY smas.servermodelapplicationserviceid ASC LIMIT 1
'''

"""
Création d'un modèle de serveur héritant d'un autre
"""
SERVERMODEL_INSERT = '''
    INSERT INTO servermodel (servermodelname, servermodeldescription, servermodelsourceid, servermodelparentsid, servermodelsubreleaseid)
        VALUES (%s, %s, %s, %s, %s)
        RETURNING servermodelid
    ;
'''

SERVERMODEL_APPLICATION_INSERT = '''
    INSERT INTO servermodelapplicationservice (servermodelid, applicationserviceid)
        VALUES (%s, %s)
    ;
'''

SERVERMODEL_APPLICATION_QUERY = '''
    SELECT * FROM servermodelapplicationservice WHERE servermodelid=%s AND applicationserviceid=%s;
'''

SERVERMODEL_APPLICATIONS = '''
    SELECT applicationserviceid FROM servermodelapplicationservice WHERE servermodelid=%s;
'''

"""
Mise à jour d'un modèle de serveur
"""
SERVERMODEL_UPDATE = '''
    UPDATE servermodel
    SET servermodelname = %s
    WHERE servermodelid = %s
    ;
'''

"""
Suppression d'un modèle de serveur
"""
SERVERMODEL_DELETE = '''
    DELETE
    FROM servermodel
    WHERE servermodelid = %s
    ;
'''

"""
Suppression du contenu de la table ServerModel
"""
SERVERMODEL_ERASE = '''
    DELETE
    FROM servermodel
'''

SERVERMODEL_FETCH_BY_NAME = '''
SELECT servermodelid FROM servermodel WHERE servermodelname = %s AND servermodelsourceid = %s AND servermodelsubreleaseid = %s;
'''

"""
Suppression du contenu de la table de jointure servermodelapplicationservice
"""
SERVERMODELAPPLICATIONSERVICE_ERASE = """DELETE from servermodelapplicationservice"""


def erase_servermodel(cursor):
    """
    Supprime le contenu de la table ServerModel
    """
    cursor.execute(SERVERMODELAPPLICATIONSERVICE_ERASE)
    cursor.execute(SERVERMODEL_ERASE)


def insert_servermodel(cursor, name, description, sourceid, parentid, subreleaseid):
    """
    Crée un modèle de serveur
    """
    cursor.execute(SERVERMODEL_INSERT, (name, description, sourceid, parentid, subreleaseid))
    return cursor.fetchone()


def join_servermodel_application(cursor, servermodelid, applicationserviceid):
    cursor.execute(SERVERMODEL_APPLICATION_QUERY, (servermodelid, applicationserviceid))
    if not cursor.fetchone():
        cursor.execute(SERVERMODEL_APPLICATION_INSERT, (servermodelid, applicationserviceid))


def get_applicationservices_by_servermodel(cursor, servermodelid):
    cursor.execute(SERVERMODEL_APPLICATIONS, (servermodelid,))
    return cursor.fetchone()


def update_servermodel(cursor, servermodelid, name):
    """
    Met à jour un modèle de serveur
    """
    return cursor.execute(SERVERMODEL_UPDATE, servermodelid, name)


def delete_servermodel(cursor, servermodelid):
    """
    Supprime un modèle de serveur
    """
    return cursor.execute(SERVERMODEL_DELETE, servermodelid)


def fetch_servermodel_list(cursor):
    """
    Récupère la liste des modèles de serveur depuis la base de données
    """
    cursor.execute(SERVERMODEL_FETCH_ALL_QUERY)
    return cursor.fetchall()

def fetch_servermodel_list_by_source(cursor, sourceid):
    """
    Récupère la liste des modèles de serveur d'une source depuis la base de données
    """
    cursor.execute(SERVERMODEL_FETCH_ALL_BY_SOURCE_QUERY, (sourceid, ))
    return cursor.fetchall()

def fetch_servermodel_by_name(cursor, name: str, sourceid: int, releaseid: int) -> int:
    """
    Retrieves a server model object by his name from the database

    """
    cursor.execute(SERVERMODEL_FETCH_BY_NAME, (name, sourceid, releaseid))
    result = cursor.fetchone()
    if result is None:
        raise ServermodelError(_("unable to find servermodel by name {}").format(name))
    return result[0]


def fetch_servermodel_simple_by_id(cursor, servermodel_id: int) -> tuple:
    """
    Récupère un modèle de serveur depuis la base de données
    """
    cursor.execute(SERVERMODEL_FETCH_SERVERMODELID_BY_ID, (servermodel_id,))
    servermodel = cursor.fetchone()
    if servermodel is None:
        raise ServermodelError(_("unable to find ID's servermodel {}").format(servermodel_id))
    return servermodel


def servermodel_by_data(cursor, name, subrelease, source):
    """
    Récupère les attributs d'un modèle de serveur dans la base de données
    """
    cursor.execute(SERVERMODEL_FETCH_SERVERMODEL_BY_DATA, (name, subrelease, source))
    servermodel = cursor.fetchone()
    if servermodel is None:
        raise ServermodelError(_('unable to find servermodel {} for subrelease {} and source {}').format(name, subrelease, source))
    return servermodel

#def servermodel_by_parent(servermodelid):
#    """
#    Récupère les attributs des modèles de serveur ayant pour parent ce servermodel
#    """
#    cursor = self.cursorect_db()
#    row = await conn.fetch(self.servermodel_fetch_servermodelid_by_servermodelparentsid, servermodelid)
#    await conn.close()
#    return row

#def fetch_severmodel_by_id(servermodel_id):
#    """
#    Récupère un modèle de serveur via son identifiant
#    """
#    conn = self.connect_db()
#    row = await conn.fetchrow(self.servermodel_fetch_one_query, servermodel_id)
#    await conn.close()
#    return row


def fetch_servermodel_applicationservices(cursor, servermodel_id: int, inheritance: bool):
    """
    Récupère les services applicatifs lié à un modèle de serveur.
    Si l'argument 'inheritance' est passé à True, les services applicatifs hérités
    des parents seront également récupérés.
    """
    if inheritance:
        cursor.execute(APPLICATIONSERVICE_FETCH_SERVELMODELS_SERVICES_INHERITANCE_QUERY, (servermodel_id,))
    else:
        cursor.execute(APPLICATIONSERVICE_FETCH_SERVELMODELS_SERVICES_QUERY, (servermodel_id,))
    return cursor.fetchall()


def fetch_servermodel_applicationservice(cursor, servermodel_id: int):
    cursor.execute(APPLICATIONSERVICE_FETCH_SERVELMODEL_QUERY, (servermodel_id,))
    return cursor.fetchone()[0]


def fetch_applicationservices_documents(cursor, applicationserviceids: list, documenttype: str):
    cursor.execute(DOCUMENT_FROM_APPLICATIONSERVICES, (documenttype, tuple(applicationserviceids),))
    return cursor.fetchall()


def fetch_information_applicationservice_document(cursor, applicationservice_id):
    """
    retourne les informations d'un document lié à un application service
    """
    cursor.execute(DOCUMENT_SHASUM_ID_FROM_APPLICATIONSERVICE, (applicationservice_id,))
    return cursor.fetchall()

def fetch_servermodel_by_parent(cursor, parentid: int):
    """
    retourne les informations d'un document lié à un application service
    """
    cursor.execute(SERVERMODEL_FETCH_BY_PARENT, (parentid,))
    return cursor.fetchall()
