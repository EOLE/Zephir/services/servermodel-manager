from os import listdir, path
from pytest import fixture, raises
import yaml
from zephir.config import ServiceConfig
from zephir.database import connect
from servermodel.applicationservice import ApplicationService
from servermodel.source import Source
from servermodel.release import Release, Version, SubRelease
from servermodel.servermodel import Servermodel
from servermodel.document.query import erase_document
from util import setup_module, teardown_module
from psycopg2.extras import DictCursor

TEST_ROOT_DIR = '40_depends/data/list_services'


class MockApplicationService(ApplicationService):
    def __init__(self):
        super(MockApplicationService, self).__init__()

    def get_dictionaries(self, src_path: str) -> str:
        return [("dico", b"toto")]

    def get_templates(self, src_path: str) -> str:
        return [("template", b"toto")]

    def get_file(self, src_path: str) -> str:
        return [("document", b"toto")]


def setup_function(function):
    global source_id, version_id, release_id_5, release_id_6, subrelease_id_5, subrelease_id_6
    cursor = CONN.cursor()
    source = Source()
    version = Version()
    release = Release()
    subrelease = SubRelease()
    applicationservice = ApplicationService()
    servermodel = Servermodel()
    source.erase_source(cursor)
    version.erase_version(cursor)
    source_id = source.create_source(cursor, 'source_test', 'htt://source_url')
    version_id = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    release_id_5 = release.create_release(cursor, version_id, '2.5.2')
    release_id_6 = release.create_release(cursor, version_id, '2.6.2')
    subrelease_id_5 = subrelease.create_subrelease(cursor, release_id_5, '2.5.2')
    subrelease_id_6 = subrelease.create_subrelease(cursor, release_id_6, '2.6.2')
    applicationservice = ApplicationService()
    servermodel = Servermodel()
    applicationservice.erase_applicationservice(cursor)
    servermodel.erase_servermodel(cursor)
    CONN.commit()
    cursor.close()


def test_list_services_252():
    cursor = CONN.cursor(cursor_factory=DictCursor)
    release = '2.5.2'
    applicationservice = MockApplicationService()
    data_path = path.join(TEST_ROOT_DIR, release, 'data{}.yml')
    num = 0
    while(True):
        filename = data_path.format(num)
        if not path.isfile(filename):
            break

        with open(filename, 'r') as data_file:
            applicationservice.process_yaml(cursor, release, subrelease_id_5, data_file.read(), filename)
        num += 1
    applicationservice.insert_applicationservice(cursor, source_id)
    applicationservice.insert_dependencies(cursor)
    applicationservice.insert_provides(cursor)

    res = applicationservice.list_services(cursor, subrelease_id_5)
    res2 = applicationservice.list_services(cursor, subrelease_id_5)

    assert len(list(res2)) == 7
    service_per_release = []

    for service in res:
        assert set(service.keys()) == set(['name', 'id', 'description'])
        assert isinstance(service['id'], int)
        service_per_release.append({'name': service['name'],
                                                 'description': service['description']})
    # check description for differents services
    for service in service_per_release:
        if service['name'] == 'apache-server':
            assert service['description'] == 'Apache est un serveur HTTP'
        elif service['name'] == 'mta-exim':
            assert service['description'] == 'Exim est un serveur de messagerie électronique (ou Mail Transfer Agent en anglais)'
        elif service['name'] == 'php-mysql':
            assert service['description'] == 'MySQL plugin for php'
        elif service['name'] == 'php-postgresql':
            assert service['description'] == 'PostgreSQL plugin for php'
        elif service['name'] == 'roundcube':
            assert service['description'] == 'RoundCube est un client webmail Open Source'
        elif service['name'] == 'nginx-server':
            assert service['description'] == 'Nginx est un serveur HTTP'
        elif service['name'] == 'php':
            assert service['description'] == 'PHP Interpreter'
        elif service['name'] == 'mta-sendmail':
            assert service['description'] == 'sendmail est un serveur de messagerie électronique (ou Mail Transfer Agent en anglais)'
        else:
            raise Exception('unknown service {}'.format(service['name']))

    assert set([s["name"] for s in service_per_release]) == set(['apache-server', 'mta-sendmail', 'php-mysql', 'php-postgresql', 'roundcube', 'nginx-server', 'php'])
    CONN.commit()
    cursor.close()


def test_list_services_262():
    cursor = CONN.cursor(cursor_factory=DictCursor)
    release = '2.6.2'
    applicationservice = MockApplicationService()
    data_path = path.join(TEST_ROOT_DIR, release, 'data{}.yml')
    num = 0
    while(True):
        filename = data_path.format(num)
        if not path.isfile(filename):
            break

        with open(filename, 'r') as data_file:
            applicationservice.process_yaml(cursor, release, subrelease_id_6, data_file.read(), filename)
        num += 1

    applicationservice.insert_applicationservice(cursor, source_id)
    applicationservice.insert_dependencies(cursor)
    applicationservice.insert_provides(cursor)

    res = applicationservice.list_services(cursor, subrelease_id_6)
    res2 = applicationservice.list_services(cursor, subrelease_id_6)

    assert len(list(res2)) == 7
    service_per_release = []

    for service in res:
        assert set(service.keys()) == set(['name', 'id', 'description'])
        assert isinstance(service['id'], int)
        service_per_release.append({'name': service['name'],
                                                 'description': service['description']})

    # check description for differents services
    for service in service_per_release:
        if service['name'] == 'apache-server':
            assert service['description'] == 'Apache est un serveur HTTP'
        elif service['name'] == 'mta-exim':
            assert service['description'] == 'Exim est un serveur de messagerie électronique (ou Mail Transfer Agent en anglais)'
        elif service['name'] == 'php-mysql':
            assert service['description'] == 'MySQL plugin for php'
        elif service['name'] == 'php-postgresql':
            assert service['description'] == 'PostgreSQL plugin for php'
        elif service['name'] == 'roundcube':
            assert service['description'] == 'RoundCube est un client webmail Open Source'
        elif service['name'] == 'nginx-server':
            assert service['description'] == 'Nginx est un serveur HTTP'
        elif service['name'] == 'php':
            assert service['description'] == 'PHP Interpreter'
        elif service['name'] == 'mta-sendmail':
            assert service['description'] == 'sendmail est un serveur de messagerie électronique (ou Mail Transfer Agent en anglais)'
        else:
            raise Exception('unknown service {}'.format(service['name']))

    assert set([s["name"] for s in service_per_release]) == set(['apache-server', 'mta-exim', 'php-mysql', 'php-postgresql', 'roundcube', 'nginx-server', 'php'])
    CONN.commit()
    cursor.close()
