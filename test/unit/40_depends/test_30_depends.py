from os import listdir, path
from pytest import fixture, raises
import yaml
from servermodel.applicationservice import ApplicationService
from servermodel.source import Source
from servermodel.release import Release, Version, SubRelease
from servermodel.servermodel import Servermodel
from servermodel.document.query import erase_document
from util import setup_module, teardown_module
from psycopg2.extras import DictCursor

RELEASE = '2.5.2'

tests = []
TEST_ROOT_DIR = '40_depends/data/depends'
for test in listdir(TEST_ROOT_DIR):
    tests.append(path.join(TEST_ROOT_DIR, test))
tests.sort()


class MockApplicationService(ApplicationService):
    def __init__(self):
        super(MockApplicationService, self).__init__()

    def get_dictionaries(self, src_path: str) -> str:
        return [("dico", b"toto")]

    def get_templates(self, src_path: str) -> str:
        return [("template", b"toto")]

    def get_file(self, src_path: str) -> str:
        return [("document", b"toto")]


@fixture(scope="module", params=tests)
def test_dir(request):
    return request.param


def setup_function(function):
    global source_id, version_id, release_id, subrelease_id
    cursor = CONN.cursor()
    applicationservice = MockApplicationService()
    source = Source()
    version = Version()
    release = Release()
    subrelease = SubRelease()
    applicationservice = ApplicationService()
    servermodel = Servermodel()
    source.erase_source(cursor)
    version.erase_version(cursor)
    source_id = source.create_source(cursor, 'source_test', 'htt://source_url')
    version_id = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    release_id = release.create_release(cursor, version_id, RELEASE)
    subrelease_id = subrelease.create_subrelease(cursor, release_id, RELEASE)
    servermodel = Servermodel()
    applicationservice.erase_applicationservice(cursor)
    servermodel.erase_servermodel(cursor)
    CONN.commit()
    cursor.close()


def test_dependencies(test_dir):
    cursor = CONN.cursor(cursor_factory=DictCursor)
    applicationservice = MockApplicationService()
    servermodel = Servermodel()

    data_path = path.join(test_dir, 'data{}.yml')
    return_path = path.join(test_dir, 'return.yml')
    if not path.isfile(return_path):
        print('Unable to load {}'.format(return_path))
        return
    with open(return_path, 'r') as return_file:
        options = yaml.load(return_file.read())

    num = 0
    while(True):
        filename = data_path.format(num)
        if not path.isfile(filename):
            break
        with open(filename, 'r') as data_file:
            applicationservice.process_yaml(cursor, RELEASE, subrelease_id, data_file.read(), filename)
        num += 1
    applicationservice.insert_applicationservice(cursor, source_id)
    applicationservice.insert_dependencies(cursor)
    applicationservice.insert_provides(cursor)
    CONN.commit()
    try:
        options_id = []
        for option in options['input']:
            options_id.append(applicationservice.get_id_by_service(cursor, option, subrelease_id))
        result = list(applicationservice.get_dependencies(cursor, options_id))
    except Exception as err:
        assert err.__class__.__name__ == options['type'], 'Unexpected exception raised: {} instead of {}'.format(err.__class__.__name__, options['type'])
        assert str(err) == options['message']
    else:
        assert result.__class__.__name__ == options['type'], 'Unexpected type returned: {} instead of {}'.format(result.__class__.__name__, options['type'])
        for service1, service2 in zip(sorted(result, key=lambda x: x['name']), sorted(options['message'], key=lambda x: x['name'])):
            assert service1['name'] == service2['name']
            assert service1['description'] == service2['description']
