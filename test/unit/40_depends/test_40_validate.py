from os import listdir, path
from pytest import fixture, raises
import yaml
from servermodel.applicationservice import ApplicationService
from servermodel.source import Source
from servermodel.release import Release, SubRelease, Version, get_counter
from servermodel.servermodel import Servermodel
from servermodel.document.query import erase_document
from util import setup_module, teardown_module
from psycopg2.extras import DictCursor

RELEASE = '2.6.2'

tests = []
TEST_ROOT_DIR = '40_depends/data/validate'
for test in listdir(TEST_ROOT_DIR):
    tests.append(path.join(TEST_ROOT_DIR, test))
tests.sort()


class MockApplicationService(ApplicationService):
    def __init__(self):
        super(MockApplicationService, self).__init__()

    def get_dictionaries(self, src_path: str) -> str:
        return [("dico", b"toto")]

    def get_templates(self, src_path: str) -> str:
        return [("template", b"toto")]

    def get_file(self, src_path: str) -> str:
        return [("document", b"toto")]


@fixture(scope="module", params=tests)
def test_dir(request):
    return request.param

def setup_function(function):
    global subreleaseid, parentid, sourceid, releaseid, subrelease_name
    cursor = CONN.cursor(cursor_factory=DictCursor)
    source = Source()
    source.erase_source(cursor)
    servermodel = Servermodel()
    servermodel.erase_servermodel(cursor)
    release = Release()
    release.erase_release(cursor)
    subrelease = SubRelease()
    subrelease.erase_subrelease(cursor)
    sourceid = source.create_source(cursor, 'source_test', 'htt://source_url')
    version = Version()
    versionid = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    releaseid = release.create_release(cursor, versionid, 'release_test')
    subreleaseid = subrelease.create_subrelease(cursor, releaseid, 'release_test')
    subrelease_name = 'release_test.' + str(get_counter(cursor, releaseid) - 1)
    parentid = servermodel.create_servermodel(cursor, None, 'servermodel_parent', 'servermodel description', subreleaseid, sourceid)
    CONN.commit()
    cursor.close()


def test_validate(test_dir):
    global CONN, subreleaseid, parentid, sourceid, releaseid, subrelease_name
    cursor = CONN.cursor(cursor_factory=DictCursor)
    applicationservice = MockApplicationService()
    data_path = path.join(test_dir, 'data{}.yml')
    return_path = path.join(test_dir, 'return.yml')
    if not path.isfile(return_path):
        return
    with open(return_path, 'r') as return_file:
        options = yaml.load(return_file.read())

    num = 0
    while(True):
        filename = data_path.format(num)
        if not path.isfile(filename):
            break
        with open(filename, 'r') as data_file:
            applicationservice.process_yaml(cursor, RELEASE, subreleaseid, data_file.read(), filename)
        num += 1
    applicationservice.insert_applicationservice(cursor, sourceid)
    applicationservice.insert_dependencies(cursor)
    applicationservice.insert_provides(cursor)
    try:
        options_id = []
        for option in options['input']['services']:
            options_id.append(applicationservice.get_id_by_service(cursor, option, subreleaseid))
        new_option = applicationservice.get_id_by_service(cursor, options['input']['new_service'], subreleaseid)
        result = list(applicationservice.prospect_optional_dependencies(cursor,
                                                                        options_id,
                                                                        new_option))
    except Exception as err:
        assert err.__class__.__name__ == options['type'], 'Unexpected exception raised: {} instead of {}'.format(err.__class__.__name__, options['type'])
        assert str(err) == options['message']
    else:
        assert result.__class__.__name__ == options['type'], 'Unexpected type returned: {} instead of {}'.format(result.__class__.__name__, options['type'])
        assert len(result) == len(options['message'])
        for idx, option in enumerate(options['message']):
            for service1, service2 in zip(sorted(result[idx], key=lambda x: x['name']), sorted(option, key=lambda x: x['name'])):
                assert service1['name'] == service2['name']
                assert service1['description'] == service2['description']
    CONN.commit()
    cursor.close()
