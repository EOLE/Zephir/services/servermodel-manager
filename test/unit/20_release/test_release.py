"""
"""
from servermodel.applicationservice import ApplicationService
from servermodel.release import Release, Version, SubRelease
from servermodel.servermodel import Servermodel
from servermodel.release.lib import release_row2obj, version_row2obj, describe_release_row2obj, subrelease_row2obj
from servermodel.release.query import SUBRELEASECOUNT_QUERY
from util import setup_module, teardown_module


def setup_function(function):
    cursor = CONN.cursor()
    version = Version()
    version.erase_version(cursor)
    CONN.commit()
    cursor.close()


def test_create_version():
    cursor = CONN.cursor()
    version = Version()
    assert isinstance(version.create_version(cursor, 'version_test', 'version_distrib', 'version_label'), int)
    CONN.commit()
    cursor.close()


def test_create_version_duplicate():
    cursor = CONN.cursor()
    version = Version()
    version_id = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    retrieve_version_id = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    CONN.commit()
    assert version_id == retrieve_version_id
    cursor.close()


def test_get_version_id():
    cursor = CONN.cursor()
    version = Version()
    version_id = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    retrieve_version_id = version.get_versionid(cursor, 'version_test', 'version_distrib')
    CONN.commit()
    assert version_id == retrieve_version_id
    cursor.close()


def test_create_release():
    cursor = CONN.cursor()
    version = Version()
    version_id = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    release = Release()
    assert isinstance(release.create_release(cursor, version_id, 'release_test'), int)
    CONN.commit()
    cursor.close()


def test_create_release_duplicate():
    cursor = CONN.cursor()
    version = Version()
    version_id = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    release = Release()
    release_id = release.create_release(cursor, version_id, 'release_test')
    retrieve_release_id = release.create_release(cursor, version_id, 'release_test')
    CONN.commit()
    assert retrieve_release_id == release_id
    cursor.close()


def test_get_release_id():
    cursor = CONN.cursor()
    version = Version()
    version_id = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    release = Release()
    release_id = release.create_release(cursor, version_id, 'release_test')
    retrieve_release_id = release.get_releaseid(cursor, version_id, 'release_test')
    CONN.commit()
    assert release_id == retrieve_release_id
    cursor.close()


def test_create_subrelease():
    cursor = CONN.cursor()
    version = Version()
    version_id = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    release = Release()
    release_id = release.create_release(cursor, version_id, 'release_test')
    subrelease = SubRelease()
    assert isinstance(subrelease.create_subrelease(cursor, release_id, 'subrelease_test'), int)
    CONN.commit()
    cursor.close()


def test_get_subrelease_id():
    cursor = CONN.cursor()
    version = Version()
    version_id = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    release = Release()
    release_id = release.create_release(cursor, version_id, 'release_test')
    cursor.execute(SUBRELEASECOUNT_QUERY, (release_id,))
    cnt = cursor.fetchone()[0]
    subrelease_name = 'subrelease_test.' + str(cnt)
    subrelease = SubRelease()
    subrelease_id = subrelease.create_subrelease(cursor, release_id, 'subrelease_test')
    retrieve_subrelease_id = subrelease.get_subreleaseid(cursor, release_id, subrelease_name)
    CONN.commit()
    assert subrelease_id == retrieve_subrelease_id
    cursor.close()


def test_list_version():
    values = [('version_test', 'version_distrib', 'version_label')]
    ids = []
    version = Version()
    with CONN.cursor() as cursor:
        for entry in values:
            ids.append(version.create_version(cursor, *entry))

    with CONN.cursor() as cursor:
        version_list = version.list_version(cursor)

    assert version_list == [version_row2obj([index, *r]) for index, r in zip(ids, values)]


def test_list_release():
    version_values = [('version_test', 'version_distrib', 'version_label')]
    version_ids = []
    version = Version()
    with CONN.cursor() as cursor:
        for entry in version_values:
            version_ids.append(version.create_version(cursor, *entry))
    print("version_ids", version_ids)
    release_values = [(v, 'release_test_{}'.format(v)) for v in version_ids]
    release_ids = []
    release = Release()
    with CONN.cursor() as cursor:
        for entry in release_values:
            release_ids.append(release.create_release(cursor, *entry))

    with CONN.cursor() as cursor:
        release_list = release.list_release(cursor)
    assert release_list == [release_row2obj([index, *r]) for index, r in zip(release_ids, release_values)]


def test_list_subrelease():
    version_values = [('version_test', 'version_distrib', 'version_label')]
    version_ids = []
    version = Version()
    with CONN.cursor() as cursor:
        for entry in version_values:
            version_ids.append(version.create_version(cursor, *entry))

    release_values = [(v, 'release_test_{}'.format(v)) for v in version_ids]
    release_ids = []
    release = Release()
    with CONN.cursor() as cursor:
        for entry in release_values:
            release_ids.append(release.create_release(cursor, *entry))

        for release_id in release_ids:
            cursor.execute(SUBRELEASECOUNT_QUERY, (release_id,))
            cnt = cursor.fetchone()[0]

            subrelease_values = [(r, 'subrelease_test_{}.{}'.format(r, cnt+i)) for i, r in enumerate(release_ids)]
            subrelease_ids = []
            subrelease = SubRelease()
            for subrelease_val in subrelease_values:
                subrelease_ids.append(subrelease.create_subrelease(cursor, release_id, 'subrelease_test_{}'.format(release_id)))
            subrelease_list = subrelease.list_subrelease(cursor)

            assert subrelease_list == [subrelease_row2obj([index, *r]) for index, r in zip(subrelease_ids, subrelease_values)]


def test_describe_release():
    version_values = [('version_test', 'version_distrib', 'version_label')]
    version_ids = []
    version = Version()
    with CONN.cursor() as cursor:
        for entry in version_values:
            version_ids.append(version.create_version(cursor, *entry))

    release_values = [(v, 'release_test_{}'.format(v)) for v in version_ids]
    release_ids = []
    release = Release()
    with CONN.cursor() as cursor:
        for entry in release_values:
            release_ids.append(release.create_release(cursor, *entry))

    with CONN.cursor() as cursor:
        describe_release = release.describe_release(cursor, release_ids[0])

    expected_result = describe_release_row2obj([release_ids[0],
                                                *release_values[0][1:],
                                                *version_values[0],
                                                ])
    assert describe_release == expected_result
