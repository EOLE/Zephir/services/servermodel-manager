from servermodel.source import Source
from servermodel.release import Release, SubRelease, Version, get_counter
from servermodel.servermodel import Servermodel, ServermodelError
from copy import copy
from psycopg2.extras import DictCursor
from util import setup_module, teardown_module


def setup_function(function):
    global subreleaseid, parentid, sourceid, releaseid, subrelease_name
    cursor = CONN.cursor(cursor_factory=DictCursor)
    source = Source()
    source.erase_source(cursor)
    servermodel = Servermodel()
    servermodel.erase_servermodel(cursor)
    release = Release()
    release.erase_release(cursor)
    subrelease = SubRelease()
    subrelease.erase_subrelease(cursor)
    sourceid = source.create_source(cursor, 'source_test', 'htt://source_url')
    version = Version()
    versionid = version.create_version(cursor, 'version_test', 'version_distrib', 'version_label')
    releaseid = release.create_release(cursor, versionid, 'release_test')
    subreleaseid = subrelease.create_subrelease(cursor, releaseid, 'release_test')
    subrelease_name = 'release_test.' + str(get_counter(cursor, releaseid) - 1)
    parentid = servermodel.create_servermodel(cursor, None, 'servermodel_parent', 'servermodel description', subreleaseid, sourceid)['servermodelid']
    CONN.commit()
    cursor.close()


def test_create_servermodel():
    global CONN, subreleaseid, parentid, sourceid, releaseid, subrelease_name
    cursor = CONN.cursor(cursor_factory=DictCursor)
    servermodel = Servermodel()
    servermodelid = servermodel.create_servermodel(cursor, parentid, 'new', 'description', subreleaseid, sourceid)['servermodelid']
    servermodelid2 = servermodel.create_servermodel(cursor, parentid, 'new2', 'description2', subreleaseid, sourceid)['servermodelid']
    assert servermodelid2 == servermodelid + 1
    servermodel.erase_servermodel(cursor)
    CONN.commit()
    cursor.close()


def test_list_created_servermodel():
    global CONN, subreleaseid, parentid, sourceid, releaseid, subrelease_name
    cursor = CONN.cursor(cursor_factory=DictCursor)
    servermodel = Servermodel()
    servermodelid = servermodel.create_servermodel(cursor, parentid, 'new', 'description', subreleaseid, sourceid)['servermodelid']
    servermodelid2 = servermodel.create_servermodel(cursor, parentid, 'new2', 'description', subreleaseid, sourceid)['servermodelid']
    servermodelid3 = servermodel.create_servermodel(cursor, parentid, 'new3', 'description', subreleaseid, sourceid)['servermodelid']
    CONN.commit()
    ret = list(servermodel.list_servermodel(cursor))
    #
    smtmpl = {'servermodelname': None, 'servermodelid': None, 'servermodeldescription': None, 'subreleaseid': subreleaseid, 'sourceid': sourceid, 'subreleasename': subrelease_name}
    sm0 = copy(smtmpl)
    sm0['servermodelname'] = 'servermodel_parent'
    sm0['servermodeldescription'] = 'servermodel description'
    sm0['servermodelid'] = parentid

    sm1 = copy(smtmpl)
    sm1['servermodelparentid'] = parentid
    sm1['servermodeldescription'] = 'description'
    sm1['servermodelname'] = 'new'
    sm1['servermodelid'] = servermodelid

    sm2 = copy(smtmpl)
    sm2['servermodelparentid'] = parentid
    sm2['servermodelname'] = 'new2'
    sm2['servermodeldescription'] = 'description'
    sm2['servermodelid'] = servermodelid2

    sm3 = copy(smtmpl)
    sm3['servermodelparentid'] = parentid
    sm3['servermodelname'] = 'new3'
    sm3['servermodeldescription'] = 'description'
    sm3['servermodelid'] = servermodelid3
    assert ret == [sm1, sm2, sm3, sm0]

    cursor.close()


def test_create_servermodel_duplicate_name():
    global CONN, subreleaseid, parentid, sourceid, releaseid, subrelease_name
    cursor = CONN.cursor(cursor_factory=DictCursor)
    servermodel = Servermodel()
    servermodel.create_servermodel(cursor, parentid, 'base', 'description', subreleaseid, sourceid)
    try:
        servermodel.create_servermodel(cursor, parentid, 'base', 'description', subreleaseid, sourceid)
    except ServermodelError as err:
        assert str(err) == 'The servermodel base already exists'
    except:
        raise Exception('create_servermodel should raises')
    servermodel.erase_servermodel(cursor)
    CONN.commit()
    cursor.close()


def test_create_servermodel_duplicate_name_list():
    global CONN, subreleaseid, parentid, sourceid, releaseid, subrelease_name
    cursor = CONN.cursor(cursor_factory=DictCursor)
    servermodel = Servermodel()
    servermodelid = servermodel.create_servermodel(cursor, parentid, 'base', 'description', subreleaseid, sourceid)['servermodelid']
    try:
        servermodel.create_servermodel(cursor, parentid, 'base', 'description', subreleaseid, sourceid)
    except ServermodelError as err:
        assert str(err) == 'The servermodel base already exists'
    except:
        raise Exception('create_servermodel should raises')
    smtmpl = {'servermodelname': None, 'servermodelid': None, 'servermodeldescription': None, 'subreleaseid': subreleaseid, 'sourceid': sourceid, 'subreleasename': subrelease_name}
    sm0 = copy(smtmpl)
    sm0['servermodelname'] = 'servermodel_parent'
    sm0['servermodeldescription'] = 'servermodel description'
    sm0['servermodelid'] = parentid

    sm1 = copy(smtmpl)
    sm1['servermodelparentid'] = parentid
    sm1['servermodelname'] = 'base'
    sm1['servermodeldescription'] = 'description'
    sm1['servermodelid'] = servermodelid

    ret = list(servermodel.list_servermodel(cursor))
    assert ret == [sm1, sm0]
    servermodel.erase_servermodel(cursor)
    CONN.commit()
    cursor.close()
