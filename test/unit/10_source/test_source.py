"""
"""


from servermodel.applicationservice import ApplicationService
from servermodel.source import Source
from servermodel.source.lib import source_row2obj
from servermodel.servermodel import Servermodel
from util import setup_module, teardown_module


def setup_function(function):
    cursor = CONN.cursor()
    source = Source()
    source.erase_source(cursor)
    CONN.commit()
    cursor.close()


def test_createsource():
    cursor = CONN.cursor()
    source = Source()
    assert isinstance(source.create_source(cursor, 'source_test', 'http://source_url'), int)
    CONN.commit()
    cursor.close()


def test_createsource_duplicate():
    cursor = CONN.cursor()
    source = Source()
    source_id = source.create_source(cursor, 'source_test', 'htt://source_url')
    retrieved_source_id = source.create_source(cursor, 'source_test', 'htt://source_url')
    CONN.commit()
    cursor.close()
    assert source_id == retrieved_source_id


def test_get_source_id():
    cursor = CONN.cursor()
    source = Source()
    source_id = source.create_source(cursor, 'source_test', 'htt://source_url')
    CONN.commit()
    retrieved_source_id = source.get_sourceid(cursor, 'source_test')
    cursor.close()
    assert source_id == retrieved_source_id


def test_list_source():
    values = [('source_test', 'htt://source_url')]
    ids = []
    source = Source()
    with CONN.cursor() as cursor:
        for entry in values:
            ids.append(source.create_source(cursor, *entry))

    with CONN.cursor() as cursor:
        source_list = source.list_source(cursor)

    assert source_list == [source_row2obj([index, *r]) for index, r in zip(ids, values)]
